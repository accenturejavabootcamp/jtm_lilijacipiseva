package jtm.activity13;

import static jtm.testsuite.AllTests.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.x.protobuf.MysqlxPrepare;
import org.apache.log4j.Logger;
import org.h2.jdbc.JdbcConnection;

public class TeacherManager {
	protected Connection conn;


	public TeacherManager() throws SQLException {
		/*-
		 * TODO #1 When new TeacherManager is created, create connection to the database server:
		 * - url = "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		 * - user = "student"
		 * - pass = "Student007"
		 * Notes:
		 * 1. Use database name imported from jtm.testsuite.AllTests.database
		 * 2. Do not pass database name into url, because some statements in tests need to be executed
		 * server-wise, not just database-wise.
		 * 3. Set AutoCommit to false and use conn.commit() where necessary in other methods
		 */

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "student", "Student007");
			conn.setAutoCommit(false);
		} catch (SQLException ex){

		}
	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 *
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) throws SQLException {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "databaseXX.Teacher"
		PreparedStatement select = this.conn.prepareStatement(String.format("SELECT * FROM %s.Teacher WHERE id = ?", database));
		select.setInt(1, id);
		ResultSet rs = select.executeQuery();
		if (rs.next()){
			return new Teacher(rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"));
		}
		return new Teacher();
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 *
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) throws SQLException {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> teachers = new ArrayList<Teacher>();


		PreparedStatement select = this.conn.prepareStatement(
				String.format("SELECT * FROM %s.Teacher WHERE firstName LIKE '%%'|| ? ||'%%' and lastName LIKE '%%' || ? || '%%'", database)
		);
		select.setString(1, firstName);
		select.setString(2, lastName);
		ResultSet rs = select.executeQuery();

		while (rs.next()){
			teachers.add(new Teacher(rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName")));
		}

		return teachers;
	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 *
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		try{
			PreparedStatement insert = this.conn.prepareStatement(
					String.format("INSERT INTO %s.Teacher(firstName, lastName) VALUES (?, ?)", database)
			);
			insert.setString(1, firstName);
			insert.setString(2, lastName);

			insert.execute();
			this.conn.commit();

			return true;
		} catch (SQLException ex){
			return false;
		}
	}

	/**
	 * Insert teacher object into database
	 *
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try{
			PreparedStatement insert = this.conn.prepareStatement(
					String.format("INSERT INTO %s.Teacher(id, firstName, lastName) VALUES (?, ?, ?)", database)
			);
			insert.setInt(1, teacher.getId());
			insert.setString(2, teacher.getFirstName());
			insert.setString(3, teacher.getLastName());

			insert.execute();
			this.conn.commit();

			return true;
		} catch (SQLException ex){
			return false;
		}
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 *
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try{
			PreparedStatement update = this.conn.prepareStatement(
					String.format("UPDATE %s.Teacher SET firstName = ?, lastName = ? WHERE id = ?", database)
			);
			update.setInt(3, teacher.getId());
			update.setString(1, teacher.getFirstName());
			update.setString(2, teacher.getLastName());

			update.execute();
			this.conn.commit();

			return true;
		} catch (SQLException ex){
			return false;
		}
	}

	/**
	 * Delete an existin
	 * g Teacher in the repository with the values represented by
	 * the ID.
	 *
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try{
			PreparedStatement delete = this.conn.prepareStatement(
					String.format("DELETE FROM %s.Teacher WHERE id = ?", database)
			);
			delete.setInt(1, id);

			delete.execute();
			this.conn.commit();

			return true;
		} catch (SQLException ex){
			return false;
		}
	}

	public void closeConnecion() {
		// TODO Close connection to the database server
		try {
			this.conn.close();
		} catch (SQLException ex){

		}
	}

}
