package jtm.extra03;

import java.util.*;

public class PracticalNumbers {

	// TODO Read article https://en.wikipedia.org/wiki/Practical_number
	// Implement method, which returns practical numbers in given range
	// including
	public String getPracticalNumbers(int from, int to) {
		List<Integer> practicalNumbers = new ArrayList<Integer>();
		for (int i = from; i <= to; i++) {
			if (this.isPracticalNumbers(i))
				practicalNumbers.add(i);

		}
		return practicalNumbers.toString();
	}

	public boolean isPracticalNumbers(int number) {
		List<Integer> divisors = this.findDivisorsOfNumber(number);
		for (int i = 1; i < number; i++) {
			if (!this.findSumOfNumber(divisors, i))
				return false;
		}
		return true;
	}

	public List<Integer> findDivisorsOfNumber(int number) {// 12 : 1 2 3 4 6
		List<Integer> divisors = new ArrayList<Integer>();
		for (int i = 1; i <= number; i++) {
			if (number % i == 0) {
				divisors.add(i);
			}
		}
		return divisors;
	}

	public boolean findSumOfNumber(List<Integer> divisors, int number) {
		int sum = 0;
		for (sum = 1; sum < number - 1; sum++)
			for (int i = 0; i < divisors.size(); i++) {
				if (sum == divisors.get(i))
					return true;
				else if (sum == divisors.get(i) + divisors.get(i + 1))
					return true;
			}

		return false;
	}

}