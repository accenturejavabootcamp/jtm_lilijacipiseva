package jtm.activity01;

import org.junit.BeforeClass;

import jtm.testsuite.AllTests;

public class ConfigurationTest1 extends ConfigurationTest {

	@BeforeClass
	public static void setUserAndPassword() {
		// It is not encouraged in class, but if you work at home,
		// you can change user and password for automated test
		// here:
		AllTests.user = "student";
		AllTests.password = "Student007";
		AllTests.timeout = 10; // Default timeout for unit tests
	}

}
