package jtm.activity06;

public class Martian implements Alien, Humanoid, Cloneable {

    Object stomach;

    public Martian() {
    }


    @Override
    public void eat(Object item) {
        if (stomach == null) {
            if (item instanceof Human) {
                Human tmp = (Human) item;
                tmp.killHimself();
            }
            stomach = item;
        }

    }

    @Override
    public Object vomit() {
        Object item = stomach;
        stomach = null;
        return item;
    }

    @Override
    public int getWeight() {
        Object item = stomach;
        if (item instanceof Human) {
            Human tmp = (Human) item;
            return Alien.BirthWeight + tmp.getWeight();
        }
        if (item instanceof Martian) {
            Martian tmp = (Martian) item;
            return Alien.BirthWeight + tmp.getWeight();
        }
        if (item instanceof Integer) {
            Integer tmp = (Integer) item;
            return Alien.BirthWeight + tmp;
        }
        return Alien.BirthWeight;
    }


    @Override
    public String isAlive() {
        return "I AM IMMORTAL!";
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + getWeight() + " [" + stomach + "]";
    }

    @Override
    public String killHimself() {
        return "I AM IMMORTAL!";
    }

    @Override
    public void eat(Integer food) {
        eat((Object) food);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Martian cloneMartian = new Martian();
        Object item = stomach;
        if (item instanceof Human) {
            Human tmp = (Human) item;
           cloneMartian.stomach = tmp.clone();
        }
        if (item instanceof Martian) {
            Martian tmp = (Martian) item;
            cloneMartian.stomach = tmp.clone();
        }
        if (item instanceof Integer) {
            Integer tmp = (Integer) item;
            cloneMartian.stomach = this.stomach;
        }
        return cloneMartian;
    }
}
