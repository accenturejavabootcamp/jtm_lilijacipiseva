package jtm.activity09;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 *
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 *
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in Collections.sort(...) method to sort list of orders
 *
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders implements Iterator<Order> {
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */

	private List<Order> orders;
	private int currIndex;

	public Orders() {
		this.orders = new ArrayList<Order>();
		this.currIndex = 0;
	}

	public void add(Order item) {
		this.orders.add(item);
	}

	public List<Order> getItemsList(){
		return this.orders;
	}

	public void sort() {
		Collections.sort(this.orders);
	}

	public Set<Order> getItemsSet(){
		HashMap<String, Order> setmap = new HashMap<String, Order>();
		Collections.sort(this.orders);
		for (Order o: this.orders) {
			if (setmap.containsKey(o.name)) {
				Order mergedOrder = setmap.get(o.name);
				mergedOrder.customer = mergedOrder.customer + "," + o.customer;
				mergedOrder.count += o.count;
			} else {
				setmap.put(o.name, new Order(o.customer, o.name, o.count));
			}
		}

		return new TreeSet<Order>(setmap.values());
	}

	@Override
	public boolean hasNext() {
		return this.currIndex < this.orders.size();
	}

	@Override
	public Order next() {
		if (this.hasNext()) {
			Order o = this.orders.get(this.currIndex);
			this.currIndex += 1;
			return o;
		} else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void remove() {
		if (this.currIndex > 0) {
			this.orders.remove(this.currIndex-1);
			this.currIndex -= 1;
		} else {
			throw new IllegalStateException();
		}
	}

	@Override
	public String toString() {
		return this.orders.toString();
	}
}
